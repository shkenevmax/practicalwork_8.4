﻿// practicalwork_8.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <algorithm>
#include <memory>
#include <string>
#include <chrono>
#include <thread>

using namespace std;

template <typename Type>
class DataNode
{
public:
    DataNode(const DataNode<Type>& source);
    DataNode(Type val);

    Type DataNodeValue;
    DataNode* left = nullptr;
    DataNode* right = nullptr;

    void Insert(DataNode<Type>* Node, Type* Value);
    DataNode* Search(DataNode<Type>* Node, Type* Value);
    DataNode* GetMin(DataNode<Type>* Node);
    DataNode* GetMax(DataNode<Type>* Node);
    DataNode* Delete(DataNode<Type>* Node, Type* Value);
    void Print(DataNode<Type>* Node);
};

template<typename Type>
DataNode<Type>::DataNode(const DataNode<Type>& source)
{
    this->DataValue = source;
}

template<typename Type>
DataNode<Type>::DataNode(Type val)
{
    this->DataNodeValue = val;
}

template<typename Type>
void DataNode<Type>::Insert(DataNode<Type>* Node, Type* Value)
{
    if (*Value < Node->DataNodeValue)
    {
        if (Node->left == nullptr) Node->left = new DataNode<Type>(*Value);
        else Insert(Node->left, Value);
    }
    else if (*Value >= Node->DataNodeValue)
    {
        if (Node->right == nullptr) Node->right = new DataNode<Type>(*Value);
        else Insert(Node->right, Value);
    }
}

template<typename Type>
DataNode<Type>* DataNode<Type>::Search(DataNode<Type>* Node, Type* Value)
{
    if (Node == nullptr) return nullptr;
    if (Node->DataValue == Value) return Node;
    return (Value < Node->DataValue) ? Search(Node->left, Value) : Search(Node->right, Value);
}

template<typename Type>
DataNode<Type>* DataNode<Type>::GetMin(DataNode<Type>* Node)
{
    if (Node == nullptr) return nullptr;
    if (Node->left == nullptr) return Node;
    return GetMin(Node->left);
}

template<typename Type>
DataNode<Type>* DataNode<Type>::GetMax(DataNode<Type>* Node)
{
    if (Node == nullptr) return nullptr;
    if (Node->right == nullptr) return Node;
    return GetMax(Node->right);
}

template<typename Type>
DataNode<Type>* DataNode<Type>::Delete(DataNode<Type>* Node, Type* Value)
{
    if (Node == nullptr) return nullptr;
    else if (Value < Node->DataValue) Node->left = Delete(Node->left, Value);
    else if (Value > Node->DataValue) Node->right = Delete(Node->right, Value);
    else if (Node->left == nullptr || Node->right == nullptr)
        Node = (Node->left == nullptr) ? Node->right : Node->left;
    else
    {
        DataNode<Type> MaxInLeft = GetMax(Node->left);
        Node->DataValue = MaxInLeft.DataValue;
        Node->right = Delete(Node->right, MaxInLeft.Value);
    }
    return Node;
}

template<typename Type>
void DataNode<Type>::Print(DataNode<Type>* Node)
{
    if (Node == nullptr) return;
    Print(Node->left);
    cout << Node->DataNodeValue << endl;
    Print(Node->right);
}

template <typename Type>
class BestConteinerEver
{
private:

	// data
	Type Value;
	DataNode<Type>* DataHeap = nullptr;

	// element ptrs
	BestConteinerEver* FirstElement;
	BestConteinerEver* LastElement;
	BestConteinerEver* NextElement;

public:
	BestConteinerEver();
	BestConteinerEver(Type val);
	BestConteinerEver(const BestConteinerEver<Type>& source);

	// basic operations
	void PushBack(Type value);
	BestConteinerEver<Type>* FindElement(const Type& value);

	template <typename ParamType>
	void remove(const ParamType& value);

	void makeHeap();
    void printConteiner();
	void printHeap();

	template <typename ParamType>
	bool has(const ParamType& value);

	BestConteinerEver<Type>* Begin();
};

template<typename Type>
inline BestConteinerEver<Type>::BestConteinerEver()
{

}

template<typename Type>
BestConteinerEver<Type>::BestConteinerEver(Type val)
{
    BestConteinerEver<Type>* NewElement = (BestConteinerEver<Type>*)malloc(sizeof(Type));
    NewElement->Value = val;
    if (FirstElement == nullptr) {
        FirstElement = NewElement;
        LastElement = NewElement;
        NextElement = NewElement;
        return;
    }
}

template<typename Type>
BestConteinerEver<Type>::BestConteinerEver(const BestConteinerEver<Type>& source)
{
    
}

template <typename Type>
void BestConteinerEver<Type>::PushBack(Type value)
{
    BestConteinerEver<Type>* NewElement = (BestConteinerEver<Type>*)malloc(sizeof(Type));

    NewElement->Value = value;

    if (FirstElement == nullptr) {
        FirstElement = NewElement;
        LastElement = NewElement;
        NextElement = NewElement;
        return;
    }

    LastElement->NextElement = NewElement;
    LastElement = NewElement;
}

template<typename Type>
BestConteinerEver<Type>* BestConteinerEver<Type>::FindElement(const Type& value)
{
    if (FirstElement == nullptr) return;
    BestConteinerEver<Type>* element = FirstElement;
    while (element && element->Value != value) element = element->NextElement;
    return (element && element->Value == value) ? element : nullptr;
}

template<typename Type>
BestConteinerEver<Type>* BestConteinerEver<Type>::Begin()
{
    return FirstElement;
}

template<typename Type>
template<typename ParamType>
inline void BestConteinerEver<Type>::remove(const ParamType& value)
{
    if (FirstElement == nullptr) return;

    if (FirstElement->Value == value) {
        BestConteinerEver<Type>* temp = FirstElement;
        FirstElement = temp->NextElement;
        delete temp;
        return;
    }
    else if (LastElement->Value == value) {
        BestConteinerEver<Type>* temp = FirstElement;
        while (temp->NextElement != LastElement) temp = temp->NextElement;
        temp->NextElement = nullptr;
        delete LastElement;
        LastElement = temp;
        return;
    }

    BestConteinerEver<Type>* slow = FirstElement;
    BestConteinerEver<Type>* fast = FirstElement->NextElement;
    while (fast && fast->Value != value) {
        fast = fast->NextElement;
        slow = slow->NextElement;
    }
    if (!fast) {
        std::cout << "This element does not exist" << std::endl;
        return;
    }
    slow->NextElement = fast->NextElement;
    delete fast;
}

template<typename Type>
void BestConteinerEver<Type>::makeHeap()
{
    BestConteinerEver<Type>* element = FirstElement->NextElement;
    DataHeap = new DataNode<Type>(FirstElement->Value);

    while (element != LastElement)
    {
        DataHeap->Insert(DataHeap, &element->Value);
        element = element->NextElement;
    }
    DataHeap->Insert(DataHeap, &LastElement->Value);
}

template<typename Type>
void BestConteinerEver<Type>::printConteiner()
{
    cout << "Conteiner:" << endl;
    BestConteinerEver<Type>* temp = FirstElement;
    while (temp != LastElement)
    {
        cout << temp->Value << endl;
        temp = temp->NextElement;
    }
    cout << LastElement->Value << endl;
    cout << endl;
}

template<typename Type>
void BestConteinerEver<Type>::printHeap()
{
    cout << "Sorted heap:" << endl;
    if (DataHeap)
    {
        DataHeap->Print(DataHeap);
    }
    cout << endl;
}

template<typename Type>
template<typename ParamType>
inline bool BestConteinerEver<Type>::has(const ParamType& element)
{
    return false;
}

void isPermutation(int firstSet[], int secondSet[])
{
    bool elementFound = false;

    for (int i = 0; abs(firstSet[i]) < 10; i++)
    {
        for (int j = 0; abs(secondSet[j]) < 10; j++)
        {
            if (firstSet[i] == secondSet[j])
            {
                elementFound = true;
            }
        }

        if (elementFound)
        {
            elementFound = false;
        }
        else
        {
            cout << "There is no permutation" << endl;
            return;
        }
    }
    cout << "There is a permutation" << endl;
}

int main()
{
    BestConteinerEver<int> myContainer;

    myContainer.PushBack(34);
    myContainer.PushBack(87);
    myContainer.PushBack(12);
    myContainer.PushBack(91);
    myContainer.PushBack(14);

    myContainer.printConteiner();
    myContainer.makeHeap();
    myContainer.printHeap();

    int arr1[]{ 3, 6, 8, 2 };
    int arr2[]{ 3, 2, 8, 6 };
    int arr3[]{ 3, 6, 5, 2 };

    cout << "The first check of the permutation:" << endl;
    isPermutation(arr1, arr2);
    cout << "The second check of the permutation:" << endl;
    isPermutation(arr1, arr3);

    return 0;
}
